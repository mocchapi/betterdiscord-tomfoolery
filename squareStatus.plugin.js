//META{"name":"Example"}*//

class Example {
	// Constructor
	constructor() {
		this.initialized = false;
	}

	// Meta
	getName() { return "Remove round pfp thing"; }
	getShortName() { return "noroundpfp"; }
	getDescription() { return "gets rid of the svg thing"; }
	getVersion() { return "0.1.0"; }
	getAuthor() { return "anne"; }

	// Settings  Panel
	getSettingsPanel() {
		return "<!--Enter Settings Panel Options, just standard HTML-->";
	}
	
	do_bups() {
		console.log('boop')
		var x = document.querySelectorAll('[mask="url(#svg-mask-avatar-status-round-32)"]');

		var i = 0;
		for (i = 0; i < x.length; i++) {
 			x[i].setAttribute("mask", "");
 		}

		var x = document.querySelectorAll('[mask="url(#svg-mask-avatar-default)"]');
		var i = 0
		for (i = 0; i < x.length; i++) {
 			x[i].setAttribute("mask", "");
		}
		var x = document.querySelectorAll('[mask="url(#svg-mask-avatar-status-round-80)"]');
		var i = 0
		for (i = 0; i < x.length; i++) {
 			x[i].setAttribute("mask", "");
		}
		var x = document.querySelectorAll('[mask="url(#svg-mask-avatar-status-mobile-32)"]');
		var i = 0
		for (i = 0; i < x.length; i++) {
 			x[i].setAttribute("mask", "");
		}
	}

	do_bips() {
		var x = document.getElementsByTagName("foreignObject");
		var i = 0;
		for (i = 0; i < x.length; i++) {
			x[i].setAttribute("mask","")
		}
	}

	// Load/Unload
	load() { 
		}

	unload() { }

	// Events

	onMessage() {
		// Called when a message is received
	};

	onSwitch() {
		this.do_bops()
		// Called when a server or channel is switched
	};

	observer(e) {
		this.do_bops()
		// console.log(e)

		// raw MutationObserver event for each mutation
	};
	
	// Start/Stop
	start() {

		console.log('hi')	
		this.do_bops()
		// x.getElementsByTagName("foreignObject").setAttribute("mask", "");
	}
	   
	stop() {
		console.log('bye')
	};


	do_bops() {
		// i dont know anything about js so if you want to use one over the other, uncomment that one and comment the other one

		// just get rid of masks for pfps
		this.do_bups();

		// remove all masks (make servers not round)
		//this.do_bips();
	}
} 
